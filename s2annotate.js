/*!
 * S2annotateJS v1.0.0
 * (c) 2017 Phong Nguyen
 * Freely distributed under the MIT license.
 */
(function(root, factory) {
	if ('object' === typeof exports && 'undefined' !== typeof module) {
		module.exports = factory();
	} else if ('function' === typeof define && define.amd) {
		define(['lib/vue'], function(Vue) {
			return (root.S2Annotate = factory(Vue));
		});
	} else {
		(root.S2Annotate = factory(Vue));
	}
}(this, function(Vue) {
	'use strict';

	var APP_VERSION = '1.0.0';
	var APP_PREFIX = 's2-annotate-';
	var MARKER_CLASSNAME = APP_PREFIX + 'annotation';
	var MARKER_ELEM = 'span';
	var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var DAYS_IN_WEEK = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

	/**
	 * Check if a value is an object.
	 */
	var isObject = function(value) {
		return '[object Object]' === Object.prototype.toString.call(value);
	}

	/**
	 * Check if value is a function.
	 */
	var isFunction = function(value) {
		return 'function' === typeof value && '[object Function]' === Object.prototype.toString.call(value)
	}

	/**
	 * Check if value is an array.
	 */
	var isArray = function(value) {
		if (Array.isArray)
			return Array.isArray(value);

		return '[object Array]' === Object.prototype.toString.call(value);
	}

	/**
	 * Wrap a number into a range of min-max
	 * if {n} passes {max} then n will wrap back to {min}
	 * The opposite effect goes for when {n} is below {min}.
	 */
	var wrap = function(n, min, max) {
		if (min > max)
			throw new Error('minimum number cannot be greater than maximum');

		var r = max - min + 1;

		return ((n - min) % r + r ) % r + min;
	}

	/**
	 * Replace a node with array of nodes.
	 * {nodes} must be an array object, not array-like or DOMList object.
	 */
	var replaceNode = function(node, nodes) {
		var p = node.parentNode, i = 0, l = nodes.length;

		for (; i < l; ++i)
			p.insertBefore(nodes[i], node);

		p.removeChild(node)

		return p;
	}

	/**
	 * Check if {char} is an empty character.
	 */
	var isEmptyChar = function(char) {
		return -1 < ' \t\n\r\v'.indexOf(char);
	}

	/**
	 * Check if a string is empty.
	 */
	var isEmptyString = function(str) {
		var i = 0, l = str.length;

		for (; i < l; ++i)
			if (!isEmptyChar(str[i]))
				return false;

		return true;
	}

	/**
	 * Check if {parent} node is a parent or ancestor of {elem} node.
	 */
	var isParent = function(elem, parent) {
		while (elem = elem.parentNode)
			if (elem == parent) return true;

		return false;
	}

	/**
	 * Check if an object is an actual DOM Node object.
	 */
	var isNode = function(obj) {
		if ('object' === typeof Node)
			return obj instanceof Node;

		return obj.nodeType && 0 < obj.nodeType;
	}

	/**
	 * Check if object is an elemnent node.
	 */
	var isElement = function(obj) {
		return isNode(obj) && obj instanceof Element && obj.nodeType === document.ELEMENT_NODE;
	}

	/**
	 * Check if object is a text node.
	 */
	var isText = function(obj) {
		return isNode(obj)&& obj.nodeType === document.TEXT_NODE;
	}

	/**
	 * merge target object with source object.
	 */
	var extend = function(tar, src) {
		for (var prop in src) {
			if (tar[prop] && isObject(src[prop]))
				extend(tar[prop], src[prop])
			else
				tar[prop] = src[prop];
		}

		return tar;
	}

	/**
	 * deep clone an object.
	 */
	var clone = function(obj) {
		if (!isObject(obj) || isFunction(obj))
			return obj;

		if (isArray(obj))
			return obj.slice();

		var clonedObj = {};
		for (var k in obj)
			clonedObj[k] = clone(obj[k]);

		return clonedObj;
	}

	/**
	 * Walk through {node} and apply callback until reached {boundNode}
	 */
	function walk(node, callback, boundNode) {
		boundNode = boundNode || document.body;

		if (!isElement(boundNode))
			throw new Error('boundary node must be an element');

		// defaults to noop function.
		if (!isFunction(callback))
			callback = Function.prototype;

		while (node) {
			if (!callback.call(null, node))
				return node;

			if (node == boundNode)
				return node;

			if (node.firstChild) {
				node = node.firstChild;
				continue;
			}

			if (node.nextSibling) {
				node = node.nextSibling;
				continue;
			}

			// go through the next sibling of the parent node.
			if (node.parentNode) {
				while (node = node.parentNode) {
					if (node == boundNode)
						return node;

					if (node.nextSibling) {
						node = node.nextSibling;
						break;
					}
				}
			}
		}

		return node;
	}

	/**
	 * Serialize a document range object into a normal object.
	 * the serialized range will be relative to {container} element.
	 */
	var RangeSerializer = function(container) {

		/**
		 * Serialize range nodes into suitable format for
		 * accessing this node again when unserialized.
		 */
		var serializeNode = function(node) {
			var i = 0, result = {indexes: [], offset: 0},
				m, e, offsetComplete = false, newNode;

			while (node) {
				if (node == container)
					break;

				if (node.previousSibling) {
					newNode = node.previousSibling;
					m = isMarker(newNode);
					e = isElement(newNode);

					// if we hit an element node (current node or previous node)
					// then move the sibling index.
					if ((!m && e) || (isElement(node) && !isMarker(node))) {
						if (!offsetComplete)
							offsetComplete = true;

						i++;
					} 

					if (!offsetComplete) {
						if (m) result.offset += newNode.textContent.length;
						else if (!e) result.offset += newNode.nodeValue.length;
					}

					node = newNode;
					continue;
				}

				node = node.parentNode;
				m = isMarker(node);
				e = isElement(node);

				if (!m && e && !offsetComplete)
					offsetComplete = true;

				if (!m) {
					result.indexes.push(i);
					i = 0;
				}
			}

			return result;
		};

		/**
		 * Get node from {serializedRange}.
		 */
		var getNode = function(node, serializedRange, isEnd) {
			var getActualChild = function(node, index) {
				if (1 > index)
					return node.childNodes[0];

				var n, i = index, l = node.childNodes.length;
				for (; i < l; ++i) {
					n = node.childNodes[i];

					if (isElement(n) && !isMarker(n))
						return n;

					if (n.previousSibling && (isElement(n.previousSibling) && !isMarker(n.previousSibling)))
						return n;
				}

				return false;
			};

			var i = serializedRange.indexes.length - 1, child,
				currOffset = 0;

			for (; 0 <= i; --i) {

				// getting actual child from index ingoring any annotation markers injected.
				child = getActualChild(node, serializedRange.indexes[i]);

				if (!child)
					return [node, currOffset];

				node = child;

				// non marker element.
				if (!isMarker(child) && isElement(child))
					continue;

				walk(child, function(n) {
					if (n == container)
						return true;

					if (isText(n)) {
						var newOffset = currOffset + n.nodeValue.length;
						// if we are within the new offset that means we have found our node.
						if (newOffset > serializedRange.offset || (isEnd && newOffset >= serializedRange.offset)) {
							currOffset = serializedRange.offset - currOffset;
							node = n;

							// break out of the walk.
							return false;
						}

						currOffset = newOffset;
					}

					return true;
				}, child.parentNode);
			}

			return [node, currOffset];
		}

		var $return = Object.create(RangeSerializer.prototype);

		$return.serialize = function(range) {

			if (range.collapsed)
				return false;

			var serialized = {
				start: serializeNode(range.startContainer),
				end: serializeNode(range.endContainer)
			};

			serialized.start.offset += range.startOffset;
			serialized.end.offset += range.endOffset;

			return new SerializedRange(serialized);
		}

		$return.unserialize = function(serializedRange) {
			
			var range = document.createRange(),
				node = getNode(container, serializedRange.start);

			range.setStart(node[0], node[1]);
			node = getNode(container, serializedRange.end, true);
			range.setEnd(node[0], node[1]);

			return range;
		}

		return $return;
	};

	/**
	 * SerializedRange Object for object ensurer that {serializedRange} is a proper
	 * serializedRange object for RangeSerializer to unserialize.
	 */
	function SerializedRange(serializedRange) {

		if (!isObject(serializedRange))
			throw new Error('serialized range is not an object.');

		// validation to ensure a proper Serialized Range object.
		if (!isObject(serializedRange.start) || !isObject(serializedRange.end))
			throw new Error('serialized range must contain start and end object property');

		if (!isArray(serializedRange.start.indexes) || !isArray(serializedRange.end.indexes))
			throw new Error('serialized range start and end indexes must be and array');

		this.start = serializedRange.start;
		this.end = serializedRange.end;

		if (isNaN(this.start.offset * 1) || isNaN(this.end.offset * 1))
			throw new Error('serialized range offsets must be an integer');
	}

	/**
	 * Check if a selection is within an element.
	 */
	var isSelectionWithinElement = function(selection, elem) {
		var anchor = selection.anchorNode,
			focus  = selection.focusNode;

		if (isParent(anchor, elem) && isParent(focus, elem))
			return true;

		return false;
	}

	/**
	 * Check whether a node is an annotation marker.
	 */
	var isMarker = function(node) {
		return isElement(node) &&
		   MARKER_ELEM.toUpperCase() === node.nodeName &&
		   MARKER_CLASSNAME === node.className
	}

	/**
	 * Create annotation marker element.
	 */
	var createMarker = function(id) {

		var marker = document.createElement(MARKER_ELEM);
		marker.className = MARKER_CLASSNAME;
		marker.setAttribute('id', APP_PREFIX + id);

		return marker;
	}

	/**
	 * Get All marker elements with {id}
	 */
	var getMarkers = function(id) {
		var _id = id * 1;

		if (!isNaN(_id))
			_id = APP_PREFIX + id;

		return document.querySelectorAll('#' + _id);
	}

	/**
	 * Get an element offset position.
	 */
	var getElementPosition = function(el) {
		if (!isNode(el) && document.ELEMENT_NODE !== el.nodeType)
			return false;

		var pos = {top: 0, left: 0};

		// support for getBoundingClientRect?
		if (!el.getBoundingClientRect) {
			var rect       = el.getBoundingClientRect();
			var scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
			var scrollTop  = window.pageYOffset || document.documentElement.scrollTop;

			pos.top = rect.top + scrollTop;
			pos.left = rect.left + scrollLeft;
		} else {
			do {
				pos.top += el.offsetTop;
				pos.left += el.offsetLeft;
			} while (el = el.offsetParent);
		}

		return pos;
	}

	/**
	 * Get the text node of {node}
	 * if {right} is then the search will start upwards.
	 */
	var getTextNode = function(node, boundNode, right) {
		if (!isNode(node))
			throw new Error('node needs to be an actual DOM Node instance.');

		var isText = 3 === node.nodeType && 0 < node.nodeValue.length, _node;

		// when we reached the boundary node we also check the boundary node itself
		// for a valid text node.
		if (node == boundNode) {
			if (isText)
				return node;

			return false;
		}

		if (isText) {
			return node;
		} else if (1 === node.nodeType) {
			// node has child ?
			_node = right ? node.lastChild : node.firstChild;
			if (_node) {
				node = getTextNode(_node, boundNode);
				if (node)
					return node;
			}
		}

		// check siblings.
		_node = right ? node.previousSibling : node.nextSibling;
		if (_node)
			return getTextNode(_node, boundNode);

		return false;
	}

	/**
	 * Trim empty characters in a range.
	 */
	var trimRange = function(range, right) {
		right = !!right;

		var n = right ? range.endContainer : range.startContainer;

		// checked node is a text node?
		if (1 === n.nodeType) {
			// we should find the text node within the range.
			n = getTextNode(n, (right ? range.startContainer : range.endContainer), right)

			if (!n)
				return false;

			right ? range.setEnd(n, n.length) : range.setStart(n, 0);
		}

		var offset = right ? range.endOffset - 1 : range.startOffset;

		// check entire string for spaces only.
		var char;
		while (char = n.nodeValue[offset]) {
			if (!isEmptyChar(char)) {
				right ? range.setEnd(n, offset + 1) : range.setStart(n, offset);
				return range;
			}

			right ? offset-- : offset++;
		}

		// worst case if the checked text node is an empty text node
		// we need to futher check current node sublings.
		var dir = right ? 'previous' : 'next', _n = n[dir + 'Sibling'];
		if (!_n && n.parentNode && n.parentNode[dir + 'Sibling'])
			_n = n.parentNode[dir + 'Sibling'];

		if (_n) {
			// offset of SetEnd -- if node is element then offset is the number of node position
			// if element is text the offset is the text length.
			right ? range.setEnd(_n, (1 === _n.nodeType ? _n.childNodes.length : _n.nodeValue.length)) : range.setStart(_n, 0);

			if (!range.isCollapsed)
				return trimRange(range, right);
		}

		return false;
	}

	/**
	 * Set an element to end of range.
	 */
	var setElement2Range = function(el, range) {
		if (!isElement(el))
			throw new Error('must contain a valid element to set to range.');

		// we should process text nodes only.
		if (3 !== range.startContainer.nodeType) {
			var t = getTextNode(range.startContainer, range.endContainer);

			if (!t)
				return false;

			range.setStart(t, 0);
		}

		// we need to collapse the range to the offset of the endContainer.
		// so we can create a placeholder element at this range.
		range.collapse(false);

		var placeholder = document.createElement('span');
		range.insertNode(placeholder);

		var pos = getElementPosition(placeholder);
		placeholder.remove();

		/**
		 * after removing the span marker, we might end up with
		 * two adjacent text nodes. normalize() will help merge these text nodes as one.
		 * @link https://developer.mozilla.org/en-US/docs/Web/API/Node/normalize
		 */
		range.startContainer.parentNode.normalize();

		range.detach();

		el.style.top = pos.top + 'px';
		el.style.left = pos.left + 'px';
	}

	/**
	 * Create annotation markers from range
	 */
	var createMarkers = function(id, range) {
		if (range.collapsed)
			return false;

		var startNode   = range.startContainer,
			endNode     = range.endContainer,
			startOffset = range.startOffset,
			endOffset   = range.endOffset,
			createText  = document.createTextNode.bind(document);

		if (!isText(startNode)) {
			startNode = getTextNode(startNode);
			startOffset = 0;
		}

		if (!isText(endNode)) {
			endNode = getTextNode(endNode);
			endOffset = endNode.nodeValue.length;
		}

		while (startNode) {

			if (isText(startNode) && !isEmptyString(startNode.nodeValue)) {
				var marker = createMarker(id);

				if (startNode == endNode) {
					marker.appendChild(createText(startNode.nodeValue.slice(startOffset, endOffset)));

					var replacingNodes = [];

					if (0 < startOffset)
						replacingNodes.push(createText(startNode.nodeValue.slice(0, startOffset)));

					replacingNodes.push(marker);

					if (endNode.nodeValue.length > endOffset)
						replacingNodes.push(createText(endNode.nodeValue.slice(endOffset)));

					replaceNode(endNode, replacingNodes);

					return;
				}

				marker.appendChild(createText(startNode.nodeValue.slice(startOffset)));

				if (0 < startOffset) {
					replaceNode(startNode, [createText(startNode.nodeValue.slice(0, startOffset)), marker]);
					startOffset = 0;
				} else {
					replaceNode(startNode, [marker]);
				}
				startNode = marker;

			} else if (startNode.firstChild) {	// is an element type.
				startNode = startNode.firstChild;
				continue;
			}

			// checking siblings
			if (startNode.nextSibling) {
				startNode = startNode.nextSibling;
			} else {
				// when having no more siblings move to the parent and check their next sibling.
				while (startNode = startNode.parentNode) {
					if (startNode == endNode)
						break;

					if (startNode.nextSibling) {
						startNode = startNode.nextSibling;
						break;
					}
				}
			}

		}	// while loop

	}

	/**
	 * remove markers with {id}.
	 */
	var removeMarkers = function(id) {
		var $markers = getMarkers(id),
			i = 0,
			p = null,
			l = $markers.length;

		for (; i < l; ++i) {
			p = $markers[i].parentNode;

			var children = [];
			for (var j = 0, len = $markers[i].childNodes.length; j < len; ++j)
				children[j] = $markers[i].childNodes[j];

			replaceNode($markers[i], children);

			// normalize to remove any adjacent text nodes.
			p.normalize();
		}
	}

	var _generatedId = null;

	/**
	 * Generate an id based on time.
	 * This should be used to generate instance and not any server id
	 * to avoid duplication e.g. annotation id
	 */
	var generateId = function() {
		var n = 1459707606518;

		var id = Math.floor((Date.now() - n) * 0.001);

		if (null !== _generatedId && id === _generatedId)
			id++;

		_generatedId = id;

		return id;
	}

	/**
	 * fixing issue with partial selection of neighbouring nodes on Firefox
	 * e.g <b>hello</b> world
	 * if we make a full selection of <b>hello</b> and also partially select ' world'
	 * to a point where ' world' is not highlighted in the browser.
	 * we get endContainer as ' world' with offset 0.
	 * we should adjust the endContainer to the previous text node sibling.
	 */
	var getRange = function(selection, idx) {
		var range = selection.getRangeAt(idx);

		if (range.startContainer != range.endContainer && 3 === range.endContainer.nodeType && 0 === range.endOffset) {
			var prev = range.endContainer.previousSibling;

			if (prev) {
				var t = getTextNode(prev, document.body, true);

				t && range.setEnd(t, t.data.length);
			}
		}

		return range;
	}

	/**
	 * Wrapper for AJAX calls.
	 */
	var xhr = function(type, url, args) {

		var parse = function(response) {
			var result;

			try {
				result = JSON.parse(response.responseText);
			}
			catch (e) {
				result = response.responseText;
			}

			return [result, response];
		};

		var self = this;

		var methods = {
			success: Function.prototype,
			error: Function.prototype
		};

		var $return = {
			success: function(callback) {
				methods.success = callback
				return $return;
			},
			error: function(callback) {
				methods.error = callback;
				return $return;
			}
		};

		args = args || {};

		var rq = new (XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
		rq.open(type, url, true);
		rq.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
		// rq.setRequestHeader('Content-type', "application/x-www-form-urlencoded");

		// indicate an XMLHttpRequest request.
		rq.setRequestHeader('X-Request-With', 'XMLHttpRequest');

		rq.onreadystatechange = function() {
			if (4 === rq.readyState) {
				if (200 === rq.status)
					methods.success.apply(null, parse(rq));
				else
					methods.error.apply(null, parse(rq));
			}
		}

		if (args.beforeSend)
			args.beforeSend.apply(null, [rq]);

		var data = null;

		if (args.data)
			data = JSON.stringify(args.data);

		rq.send(data);

		return $return;
	};

	// global event bus that every event bus of an instance should listen to.
	var Bus = new Vue();

	// closing any open component of the app when clicked any element
	// that is not within app or app containers.
	document.documentElement.addEventListener('click', function(e) {
		var tar = e.target, i, l;

		var $apps = document.querySelectorAll('.' + APP_PREFIX + 'app');

		// target is one of app instances.
		for (i = 0, l = $apps.length; i < l; ++i) {
			if (tar == $apps[i] || isParent(tar, $apps[i]))
				return;
		}

		// check if target is within the containers.
		var containers = document.querySelectorAll('.' + APP_PREFIX + 'container');
		for (i = 0, l = containers.length; i < l; ++i) {
			if ((tar == containers[i] || isParent(tar, containers[i])) && !document.getSelection().isCollapsed)
				return;
		}

		Bus.$emit('handle', 'adder:close');
		Bus.$emit('handle', 'form:close');
		Bus.$emit('handle', 'app:close');

		return false;

	}, false);

	// REST terms.
	var __methods = {
		search:  'GET',
		read:    'GET',
		create:  'POST',
		update:  'PUT',
		destroy: 'DELETE'
	};

	var APP = function(options) {

		var _options = {};
		var instanceId = null;

		// root vue component
		var app = null;

		var bus = new Vue({
			created: function() {
				var self = this;

				// start listening for global Bus events.
				Bus.$on('handle', function(handler, data) {
					self.$emit(handler, data);
				});
			}
		});

		var currRange = null;
		var currSelectedMarkers = [];
		var self = this;

		/** Vue Components */

		var Adder = Vue.extend({
			template:
				'<transition name="zoom-in">'+
					'<div class="' +APP_PREFIX+ 'btn" @click="annotate" v-show="visible">'+
						'<span class="adder__icon fa fa-pencil"></span>'+
					'</div>'+
				'</transition>',
			data: function() {
				return {
					visible: false
				}
			},
			created: function() {
				var self = this;

				bus.$on('adder:open', function() {
					self.visible = true;
				});
				bus.$on('adder:close', function() {
					self.visible = false;
				});
			},
			methods: {
				annotate: function() {
					this.visible = false;
					bus.$emit('form:open');
				}
			}
		});

		var Form = Vue.extend({
			template:
				'<transition name="zoom-in">'+
					'<div class="' +APP_PREFIX+ 'form" v-show="visible">'+
						'<div class="form-header clearfix">'+
							'<div class="form-header__title title">Create Annotation</div>'+
							'<button type="button" class="form-header__close btn" @click="close"><span class="fa fa-times"></span></button>'+
						'</div>'+
						'<div class="form-content">'+
							'<textarea class="form-content__annotation" rows="10" placeholder="Write your annotation" />'+
							'<button type="button" class="form-content__create btn" @click="create">Create</button>'+
						'</div>'+
					'</div>'+
				'</transition>',
			data: function() {
				return {
					visible: false
				}
			},
			created: function() {
				bus.$on('form:open', this.open);
				bus.$on('form:close', this.close);
			},
			computed: {
				textArea: function() {
					return this.$el.querySelector('.form-content__annotation');
				}
			},
			methods: {
				open: function() {
					this.visible = true;

					this.textArea.value = '';

					this.$nextTick(function() {
						this.textArea.focus();
					});
				},

				close: function() {
					this.visible = false;
				},

				create: function() {
					if (isEmptyString(this.textArea.value)) {
						this.textArea.focus();
						return false;
					}

					if (currRange) {
						var serializedRange = RangeSerializer(_options.container).serialize(currRange.cloneRange());

						if (!serializedRange)
							throw new Error("Failed to create annotation. Could not serialize range.");

						var annotation = {
							content: this.textArea.value,
							range: serializedRange
						};

						bus.$emit('app:create', annotation);
					}

					this.close();
				}
			}
		});

		var Annotation = Vue.extend({
			template:
				'<div class="annotation" :id="createId">'+
					'<div class="annotation__text-selection">'+
						'<div class="text-selection__title title">Selected text</div>'+
						'<div class="text-selection__text">{{ refContent }}</div>'+
					'</div>'+
					'<div class="annotation__content">'+
						'<div class="annotation-title title">Annotation</div>'+
						'<div class="annotation-content" @mouseenter="showControls = true" @mouseleave="showControls = false">'+
							'<transition name="fade-down">'+
								'<div class="content__controls" v-show="showControls">'+
									'<button class="controls__edit-annotation btn" type="button" @click="doControl(\'edit\')" v-if="controls.edit" title="edit" :key="editControl">'+
										'<span class="fa fa-pencil"></span>'+
									'</button>'+
									'<button class="controls__delete-annotation btn" type="button" @click="doControl(\'delete\')" v-if="controls.delete" title="delete" :key="deleteControl">'+
										'<span class="fa fa-trash-o"></span>'+
									'</button>'+
								'</div>'+
							'</transition>'+
							'<div class="content__text" @blur="doneEditing" :contenteditable="onEditMode ? true : false">{{ content }}</div>'+
						'</div>'+
					'</div>'+
					'<div class="annotation__desc">'+
						'<div class="annotation-author" v-if="author" title="author">'+
							'<span class="fa fa-user desc__icon"></span>'+
							'<span class="desc__text">{{ author }}</span>'+
						'</div>'+
						'<div class="annotation-date-created" v-if="dateCreated" title="date created">'+
							'<span class="fa fa-calendar desc__icon"></span>'+
							'<span class="desc__text">{{ prettyDate }}</span>'+
						'</div>'+
						'<div class="annotation-date-edited" v-if="dateEdited" title="date edited">'+
							'<span class="fa fa-clock-o desc__icon"></span>'+
							'<span class="desc__text">{{ prettifyDate(dateEdited) }}</span>'+
						'</div>'+
					'</div>'+
				'</div>',
			props: {
				id: {
					type: [Number, String],
					required: true
				},
				content: {
					type: String,
					required: true
				},
				author: {
					type: String,
					default: ''
				},
				dateCreated: {
					type: [String, Number],
					default: Date.now()
				},
				range: {
					type: [Object, SerializedRange],
					required: true
				}
			},
			data: function() {
				return {
					onEditMode: false,
					editedContent: this.content,
					dateEdited: null,
					showControls: false,
					refContent: '',
					controls: _options.controls
				};
			},
			watch: {
				// if a range has changed then rebuild the annotation marker using the new range.
				range: {
					handler: function(a) {
						this.buildRange();
					},
					deep: true
				}
			},
			created: function() {
				this.buildRange();
			},
			computed: {
				prettyDate: function() {
					return this.prettifyDate(this.dateCreated);
				},

				createId: function() {
					return APP_PREFIX + 'annotation-' + this.id;
				}
			},
			methods: {
				getData: function() {
					var obj = {
						id: this.id,
						content: this.editedContent,
						refContent: this.refContent,
						author: this.author,
						dateCreated: this.dateCreated,
						prettyDates: {created: this.prettyDate},
						range: this.range
					};

					if (this.dateEdited) {
						obj.dateEdited = this.dateEdited;
						obj.prettyDates.edited = this.prettifyDate(this.dateEdited);
					}

					return obj;
				},
				prettifyDate: function(timestamp) {
					timestamp = timestamp * 1;

					if (isNaN(timestamp))
						throw new Error('prettifyDate: timestamp need to be a proper integer');

					var date = new Date(timestamp),
						d    = date.getDate();

					return DAYS_IN_WEEK[date.getDay()] + ' '+
						   (10 > d ? '0' + d : d) + ' '+
						   MONTHS[date.getMonth()] + ' '+
						   date.getFullYear();
				},
				doControl: function(name) {
					if (!_options.controls[name])
						return;

					if (this[name + 'Control'])
						this[name + 'Control']();
				},
				editControl: function() {
					var $content = this.$el.querySelector('.content__text');

					this.onEditMode = true;

					this.$nextTick(function() {
						$content.focus();

						// moving caret position to end of content.
						var selection = document.getSelection(),
							range = document.createRange();

						range.selectNodeContents($content);
						range.collapse(false);
						selection.removeAllRanges();
						selection.addRange(range);
					});
				},
				deleteControl: function() {
					if (window.confirm('Are you sure you want to delete annotaton?'))
						this.$emit('annotationDelete', this.id);
				},
				doneEditing: function() {
					var $content = this.$el.querySelector('.content__text')

					// detect changes in content
					if ($content.textContent !== this.content) {
						this.editedContent = $content.textContent;

						// update the edited date.
						this.dateEdited = Date.now();
						
						// issue with firefox creating multiple adjacent text nodes after setting caret to end
						$content.normalize();

						this.$emit('annotationEdited', this.id, this.getData());
					}

					// turn off edit mode.
					this.onEditMode = false;
				},
				buildRange: function() {
					// @note this must be called before unserializing range.
					// as removing DOM elements will mutate the range.
					removeMarkers(this.id);

					var range = RangeSerializer(_options.container).unserialize(this.range);
					if (range) {
						this.refContent = range.toString();
						createMarkers(this.id, range)
					}
				}
			}
		});

		var AppVM = Vue.extend({
			template:
				'<div class="' +APP_PREFIX+ 'app" :id="id">'+
					'<annotationAdder v-if="canAdd"></annotationAdder>'+
					'<annotationForm v-if="canAdd"></annotationForm>'+
					'<div class="annotations-panel" v-show="visible">'+
						'<div class="header clearfix">'+
							'<div class="annotation-selector" v-show="1 < selected.length">'+
								'<button type="button" class="annotation-selector__prev btn" @click="prev" title="previous annotation">'+
									'<span class="fa fa-angle-left"></span>'+
								'</button>'+
								'<button type="button" class="annotation-selector__next btn" @click="next" title="next annotation">'+
									'<span class="fa fa-angle-right"></span>'+
								'</button>'+
							'</div>'+
							'<button class="header-close btn" type="button" @click="close"><span class="fa fa-times"></span></button>'+
						'</div>'+
						'<div class="annotations-wrapper">'+
							'<annotation  v-for="annotation in annotations" '+
								'v-show="annotation.id === selected[selectedIdx]" '+
								':key="annotation.id" '+
								':id="annotation.id" '+
								':content="annotation.content" '+
								':author="annotation.author" '+
								':range="annotation.range" '+
								'@annotationDelete="remove" '+
								'@annotationEdited="update" '+
								':dateCreated="annotation.dateCreated">'+
							'</annotation>'+
						'</div>'+
					'</div>'+
				'</div>'
			,
			data: function() {
				return {
					id: null,
					annotations: [],
					selected: [],
					selectedIdx: 0,
					visible: false,
					canAdd: _options.controls.create,
					generatedId: 0
				};
			},
			components: {
				annotationAdder: Adder,
				annotationForm: Form,
				annotation: Annotation
			},
			mounted: function() {
				bus.$on('app:create', this.create);
				bus.$on('app:close', this.close);

				bus.$emit('app:mounted');
			},
			computed: {
				selectedAnnotations: function() {
					var self = this;
					return this.annotations.filter(function(annotation) {
						if (-1 < self.selected.indexOf(annotation.id * 1))
							return annotation;
					});
				}
			},
			methods: {
				// create an annotation.
				create: function(annotation, sync) {
					annotation = annotation || {};
					sync = 'undefined' === typeof sync ? true : sync;

					var tempId = (this.id + "" + ++this.generatedId) * 1;

					if (!annotation.id)
						annotation.id = tempId;
					else if (annotation.id && tempId === annotation.id)
						annotation.id++;

					// ensure that the range is a SerializedRange object.
					annotation.range = new SerializedRange(annotation.range);

					var range = RangeSerializer(_options.container).unserialize(annotation.range);

					if (!range)
						return false;

					if (!annotation.dateCreated)
						annotation.dateCreated = Date.now();

					// in non silent mode, will sync the new created annotation to server.
					if (sync && !_options.offline) {
						var that = this;
						doRequest('create', '', annotation)
						.success(function(response) {
							if (isObject(response)) {
								// already exists then abort.
								if (that.getAnnotation(response.id))
									return false;

								that.annotations.push(response);

								that.$nextTick(function() {
									bus.$emit('annotation:created', clone(response));
								});
							}
						});
					} else {
						if (this.getAnnotation(annotation.id))
							return false;

						this.annotations.push(annotation);
						this.$nextTick(function() {
							bus.$emit('annotation:created', clone(annotation));
						});
					}

				},

				// show selected annotations.
				show: function(ids) {
					if (!isArray(ids))
						return false;

					var annotations = [], annotation, i = 0, l = ids.length;
					for (; i < l; ++i)
						if (annotation = this.getAnnotation(ids[i]))
							annotations.push(ids[i]);

					if (1 > annotations.length)
						return false;

					this.selectedIdx = 0;
					this.selected = annotations;
					this.visible = true;
				},

				// close app.
				close: function() {
					this.selectedIdx = 0;
					this.selected = [];
					this.visible = false;					
				},

				// select previous/next annotation in the selected annotations list.
				prev: function() {
					this.selectedIdx = wrap(this.selectedIdx - 1, 0, this.selected.length-1);
				},
				next: function() {
					this.selectedIdx = wrap(this.selectedIdx + 1, 0, this.selected.length-1);
				},

				// remove annotation
				remove: function(id) {
					var annotation = this.getAnnotation(id);

					if (!annotation)
						return false;

					if (!_options.offline) {
						var that = this;
						doRequest('destroy', id, annotation[1])
						.success(function(response) {
							that.annotations.splice(annotation[0], 1);
						});

						that.$nextTick(function() {
							bus.$emit('annotation:deleted', clone(annotation[1]));
							removeMarkers(id);
							that.close();
						});
					} else {
						this.annotations.splice(annotation[0], 1);
						this.$nextTick(function() {
							bus.$emit('annotation:deleted', clone(annotation[1]));
							removeMarkers(id);
							this.close();
						});
					}
				},

				// update an annotation.
				update: function(id, attrs, sync) {
					sync = 'undefined' === typeof sync ? true : sync;

					var annotation = this.getAnnotation(id);

					if (!annotation)
						return false;

					if (attrs.id) {
						if (isNaN(attrs.id * 1))
							throw new Error('annotation id must be a proper integer');

						if (attrs.id !== id && this.getAnnotation(attrs.id))
							throw new Error('id already belongs to another annotation.')
					}

					if (attrs.range)
						attrs.range = new SerializedRange(attrs.range);

					extend(annotation[1], attrs);

					this.$set(this.annotations, annotation[0], annotation[1]);

					this.$nextTick(function() {
						var _annotation = clone(annotation[1]);

						// sync to server.
						if (!_options.offline) {
							doRequest('update', _annotation.id, _annotation)
							.success(function() {
								bus.$emit('annotation:updated', _annotation);
							});
						} else {
							bus.$emit('annotation:updated', _annotation);
						}
					});
				},

				// get annotations from server.
				fetch: function() {
					if (_options.offline)
						return;

					var that = this;
					doRequest('search')
					.success(function(response) {
						if (!isArray(response))
							return;

						var i = 0, l = response.length;
						for (; i < l; ++i) {
							if (!that.getAnnotation(response[i].id))
								that.create(response[i], false);
							else
								that.update(response[i].id, response[i], false);
						}

						bus.$emit('annotations:fetched');
					})
					.error(function() { });
				},
				getAnnotation: function(id) {
					id = id * 1;

					var i = 0, l = this.annotations.length, a;

					for (; i < l; ++i)
						if (this.annotations[i].id === id)
							return [i, this.annotations[i]];

					return false;
				}
			}
		});

		var initialize = function() {
			if (!options || !isObject(options))
				options = {};

			_options = processOptions(options);

			_options.container.classList.add(APP_PREFIX + 'container');

			instanceId = generateId();

			renderAppView();

			addNativeEvents();

			app.fetch();
		};

		var processOptions = function(options) {
			// requires valid element container for annotating.
			if (!options.container)
				throw new Error('No container element passed. please set a valid element selector or an element node');

			// passed a string selector as the container?
			if ('string' === typeof options.container || options.container instanceof String)
				options.container = document.querySelector(options.container)

			if (!isElement(options.container) || !document.body.contains(options.container))
				throw new Error('provided element container does not exist.');

			// boolean conversion
			if (options.hasOwnProperty('offline'))
				options.offline = !!options.offline;

			var df = {
				baseUrl: '/s2annotate',
				urls:  {
					search: '/annotations',
					read: '/annotations/{id}',
					create: '/annotations',
					update: '/annotations/{id}',
					destroy: '/annotations/{id}'
				},
				controls: {
					create: true,
					edit: true,
					delete: true
				},
				beforeSend: Function.prototype,
				offline: false
			};

			var o = clone(df);
			extend(o, options);

			// post processing options.

			if (!o.offline) {
				if ('string' !== typeof o.baseUrl)
					throw new Error('base url needs to be a string containing the base API url');

				if (isEmptyString(o.baseUrl))
					o.baseUrl = df.baseUrl;

				if (!isObject(o.urls))
					throw new Error('API endpoint urls need to be an object with urls for [search, read, create, update, destroy] props');

				// empty object of urls.
				if (0 === Object.keys(o.urls).length)
					o.urls = df.urls;
			}

			if (!isObject(o.controls)) {
				throw new Error(
					'controls option must be an object containing [create, edit, delete] properties with true/false indicating' +
					'the availability of these controls'
				);
			}

			if (0 === Object.keys(o.controls).length)
				o.controls = df.controls;

			if (!isFunction(o.beforeSend))
				throw new Error('beforeSend option must be a function');

			return o;
		}

		var renderAppView = function() {
			if (app && app._isMounted)
				return false;

			app = new Vue(AppVM);

			app.id = instanceId;

			var placeholder = document.createElement('div');
			document.body.appendChild(placeholder);

			app.$mount(placeholder);
		}

		var _mouseUp = function(e) {
			currRange = null;
			
			Bus.$emit('handle', 'adder:close');
			Bus.$emit('handle', 'form:close')
			var selection = document.getSelection();

			// no selection made.
			if (selection.isCollapsed)
				return false;

			// check selection is within container element.
			if (!isSelectionWithinElement(selection, _options.container))
				return false;

			var text = selection.toString();

			if (isEmptyString(text))
				return false;

			var range = getRange(selection, 0);

			if (!range)
				return false;

			// trim front and trailing spaces of selection.
			range = trimRange(range);
			range && (range = trimRange(range, true));

			// update the range selection.
			selection.removeAllRanges();

			range && selection.addRange(range);

			// keep reference of current range.
			var _range = range.cloneRange();

			setElement2Range(app.$el, _range);
			bus.$emit('adder:open')

			currRange = range;

			return false;
		}

		var _mouseOver = function(e) {

			var tar = e.target,
				markers = [];

			while (tar) {
				if (tar == this)
					break;

				if (isMarker(tar))
					markers.push(tar.id)

				tar = tar.parentNode;
			}

			if (0 < markers.length) {
				for (var i = 0, l = markers.length, $markers; i < l; ++i) {
					$markers = document.querySelectorAll('#' + markers[i]);

					for (var j = 0, len = $markers.length; j < len; ++j)
						$markers[j].setAttribute('data-onhover', true);
				}
			}

			currSelectedMarkers = markers;

			return false;
		}

		var _mouseOut = function(e) {
			var markers = document.querySelectorAll('[data-onhover]');

			if (0 < markers.length)
				for (var i = 0, l = markers.length; i < l; ++i)
					markers[i].removeAttribute('data-onhover');

			currSelectedMarkers = [];

			return false;
		}

		var _onClick = function(e) {
			e.stopPropagation();

			Bus.$emit('handle', 'app:close');

			if (1 > currSelectedMarkers.length || currRange)
				return;

			app.$nextTick(function() {
				var selected = [];
				for (var i = 0, l = currSelectedMarkers.length; i < l; ++i)
					selected.push(currSelectedMarkers[i].split(APP_PREFIX)[1] * 1)

				var selection = document.getSelection();

				if (selection)
					setElement2Range(app.$el, selection.getRangeAt(0));

				this.show(selected);
			});
		}

		var addNativeEvents = function() {
			if (_options.container) {
				_options.container.onmouseup = _mouseUp;

				_options.container.onmouseover = _mouseOver;

				_options.container.onmouseout = _mouseOut;

				_options.container.onclick = _onClick;
			}
		}

		function createUrl(type, id) {
			var url = _options.baseUrl;

			if (_options.urls[type])
				url += _options.urls[type];

			// replace `id` placeholders.
			url = url.replace('{id}', (id ? id : ''));

			// trim trailing slash.
			url = url.replace(/\/$/, '');

			return url;
		}

		// data should accept success and error callback
		function doRequest(type, id, data) {
			var url = createUrl(type, id ? id : null),
				xhrData = {};

			if (isFunction(_options.beforeSend))
				xhrData.beforeSend = _options.beforeSend;

			if (isObject(data))
				xhrData.data = data;

			return xhr(__methods[type], url, xhrData)
		}

		initialize();

		self.on = function(handler, fn) {
			bus.$on(handler, fn.bind(self));
		};

		self.off = function(handler) {
			bus.$off(handler);
		};

		self.createAnnotation = function(annotation) {
			if (!isObject(annotation))
				return false;

			app.create(annotation);
		};

		self.deleteAnnotation = function(id) {
			id = id * 1;

			if (isNaN(id))
				return false;

			app.remove(id);
		};

		self.updateAnnotation = function(id, attrs) {
			id = id * 1;

			if (isNaN(id))
				return false;

			app.update(id, attrs);
		};

		self.serializeRange = function(range, container) {
			return RangeSerializer(container).serialize(range);
		};

		self.unserializeRange = function(serializedRange, container) {
			return RangeSerializer(container).unserialize(serializedRange);
		}

		return self;
	}

	return APP;
}));