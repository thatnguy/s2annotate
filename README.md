# S2AnnotateJS (Simple 2 Annotate) #

A simple lightweight annotation app.

### Requirements ###

* VueJS @ [here](https://vuejs.org)
* FontAwesome icons @ [here](http://fontawesome.io/icons)

### options ###
* *container <string|DOM Element>* - An existing element that the app is activated on. This can be a element selector or an Element Node (required).
* *baseUrl <string>* - define a baseUrl for API calls. defaults to '/s2annotate'
* *urls (optional)* - define the API endpoint urls. This must be an array containing the url string for 'search', 'read', 'create', 'update', 'destroy' (optional).
* *controls <object>* - set the availability of annotation controls (create, edit, delete). Must supply an object defining whether to enable control (optional).
* *beforeSend <function>* - a callback that will be called each time before app makes request to server. XMLHttpRequest object is passed as argument (optional).
* *offline <boolean>* - whether to use app in offline mode. Offline mode will only operate on the client and not make any request or sync to server (optional).

e.g.

```
#!javascript

var options = {
   container: '#main-content',
   baseUrl: 'wp-json/myannotations',
   urls: {

   },
   controls: {
     create: true,
     edit: false,
     delete: false
   },
   beforeRequest: function(xhr) {
      xhr.setRequestHeader('X-WP-TOKEN', '..');
   },
   offline: false
};
```



### Events ###
* *annotation:created* - emitted when an annotation is created
* *annotation:deleted* - emitted when an annotation is deleted
* *annotation:updated* - emitted when an annotation is updated

### example ###
please check out the demo.html file.